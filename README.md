### Pre-built toolchains for working with the AArch64 port of Redox
***

This repository holds pre-built toolchains in the following forms:

* Raw binary tarballs

The toolchains are of 2 types:

* binutils + GCC
* rustc

#### Instructions for using the Raw binary tarballs
***
Unpack the tarballs at a suitable location and update the **PATH** environment variable.
```
mkdir toolchains && cd toolchains
wget https://gitlab.redox-os.org/microcolonel/aarch64-redox-binary-toolchain-snapshots.git/aarch64-unknown-redox-binutils-and-gcc-19082018-01.tar.bz2 -O - | tar -jxvf -
export PATH=$PWD/prefix/bin:$PATH
wget
https://gitlab.redox-os.org/microcolonel/aarch64-redox-binary-toolchain-snapshots.git/rustc-stage2-aarch64-and-x86_64-unknown-redox-19082018-01.tar.bz2 -O - | tar -jxvf -
export PATH=$PWD/stage2/bin:$PATH
```

**rustc** and **aarch64-unknown-redox-gcc** (among other tools) should now be available and usable
